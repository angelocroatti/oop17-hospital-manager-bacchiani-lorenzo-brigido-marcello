package model;

import utilities.Role;

public interface Worker extends Person {

	/**
	 * 
	 * @return the role the person.
	 */
	Role getRole();

}
